package com.qytest.springcloud.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

/**
 * @author zhaoqiuyue
 * @date 2022年07月19日 16:24
 */
@RestController
@Slf4j
public class PaymentController {
    @Value("${server.port}")
    private String serverport;

    @GetMapping(value = "/payment3/zk")
    public String paymentzk() {
        return "zk注册中心测试:" + serverport + "\t" + UUID.randomUUID().toString();
    }
}
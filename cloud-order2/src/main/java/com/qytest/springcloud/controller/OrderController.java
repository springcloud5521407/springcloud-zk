package com.qytest.springcloud.controller;

import com.qytest.springcloud.entities.CommonResult;
import com.qytest.springcloud.entities.Payment;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @author qy
 * @date 2022年07月13日 16:52
 */
@RestController
@RequestMapping("/consumer")
@Slf4j
public class OrderController {
    //public static final String PAYMENT_URL = "http://localhost:8001";
    public static final String PAYMENT_URL = "http://cloud-PAYMENT";
    @Resource
    private RestTemplate restTemplate;

    @PostMapping("/order")
    public CommonResult<Payment> createOrder(@RequestBody Payment payment) {
        return restTemplate.postForObject(PAYMENT_URL + "/payment", payment, CommonResult.class);
    }

    @GetMapping("/order/{id}")
    public CommonResult<Payment> getOrderPayment(@PathVariable("id") Long id) {
        return restTemplate.getForObject(PAYMENT_URL + "/payment/" + id, CommonResult.class);
    }
}
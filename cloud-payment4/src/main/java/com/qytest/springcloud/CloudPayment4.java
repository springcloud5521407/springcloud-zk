package com.qytest.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author qy
 * @date 2022年07月19日 16:12
 */
@SpringBootApplication
@EnableDiscoveryClient
public class CloudPayment4 {
    public static void main(String[] args) {
        SpringApplication.run(CloudPayment4.class);
    }
}
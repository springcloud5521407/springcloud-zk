package com.qytest.springcloud.controller;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;


@RestController
@Slf4j
public class PaymentController {
    @Value("${server.port}")
    private String serverport;

    @GetMapping(value = "/payment4/consule")
    public String paymentzk() {
        return "consule注册中心测试:" + serverport
                + "\t" + UUID.randomUUID().toString();
    }
}